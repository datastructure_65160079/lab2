package lab2;

public class RemoveElement {
    public static int remove(int[] arr,int val){
        int k =0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]!=val) {
                arr[k] =arr[i];
                k++;
            }
        }
        return k;
    }
    public static void main(String[] args) {
        int[] arr = {3,2,2,3};
        int val = 3;
        int newarr = remove(arr, val);
        System.out.println(newarr);
    }
}
